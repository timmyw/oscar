all: oscar

.phony: clean debug

OSTYPE=$(shell uname -s)
ASM=nasm
ASMFLAGS=-D $(OSTYPE) -f elf64 -g -F dwarf
LD=ld
LDFLAGS=-nopie -g

TEST_EXEC=$(patsubst %.asm,%,$(wildcard tests/*.asm))

OBJS=obj/oscar.o obj/termios.o obj/sys.o \
	obj/io.o obj/editor.o obj/exit.o obj/data.o \
	obj/string.o

obj/%.o : %.asm
	mkdir -p obj lst
	$(ASM) $(ASMFLAGS) -l lst/$<.lst $< -o $@

tests/obj/%.o : tests/%.asm
	mkdir -p tests/obj tests/lst
	$(ASM) $(ASMFLAGS) -l $<.lst $< -o $@


oscar: $(OBJS)
	$(LD) $(LDFLAGS) -o $@ $(OBJS)

debug: oscar
	egdb -tui ./oscar

test: $(TEST_EXEC)
	@for x in $(TEST_EXEC); \
	do  \
		echo $$x ; \
		./$$x ; \
	done

tests/test_editor: obj/editor.o obj/termios.o obj/sys.o obj/data.o obj/string.o obj/io.o obj/exit.o tests/obj/test_editor.o
	$(LD) $(LDFLAGS) -o $@ $^

tests/test_string: obj/string.o obj/io.o tests/obj/test_string.o
	$(LD) $(LDFLAGS) -o $@ $^

clean:
	rm -f *.o *.lst obj/*.o lst/*.lst
	rm -f oscar termios
	rm -f test_string

