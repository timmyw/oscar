;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Contains all the global data for the editor
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

	%include "data.def"
	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

	section .bss

;;; struct termios
	termios_c_iflag  resb 4
	termios_c_oflag  resb 4
	termios_c_cflag  resb 4
	termios_c_lflag  resb 4
	termios_c_cc     resb 20
	termios_c_ispeed resb 4
	termios_c_ospeed resb 4

	ssize_termios_struct equ $-termios_c_iflag 

	;; Space for a copy of the original terminal attributes
	termios_org resb ssize_termios_struct

;;; struct winsize (from sys/ttycom.h)
	winsize_ws_row		resb 2
	winsize_ws_col		resb 2
	winsize_ws_xpixel	resb 2
	winsize_ws_ypixel	resb 2
	
	ssize_winsize_struct equ $-winsize_ws_row 

;;; Editor buffers
	output_buffer	resb 1024

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

	section .data

	ed_tilde	db	"~", 13, 10, 0
	

