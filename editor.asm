;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Contains editor functions
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

	%include "io.def"
	%include "editor.def"
	%include "exit.def"
	%include "termios.def"
	%include "data.def"
	%include "string.def"

	%include "macro.def"
	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

	section .text

init_editor:
	call	getwinsz			; rows/cols in rax
	dec	word ax
	mov	[screen_max_y], word ax
	shr	rax, 0x10
	dec	word ax
	mov	[screen_max_x], word ax
init_editor_0:
	ret

refresh_screen:
	call	ob_init_buffer
	mov	rsi, esc_cursor_off ; Cursor off while we redraw the screen
	call	ob_append_buffer
	
	call	editor_cls			; Clear the screen
	call	editor_homecursor		; Jump to the top
	call	draw_rows			; Draw all the rows

	call	editor_show_pos
	
	call	editor_movecursor_current	; 

	mov	rsi, esc_cursor_on	; Cursor back on
	call	ob_append_buffer

	;; Move cursor to current position

	call	ob_output_buffer
	ret

editor_movecursor_current:
	xor	rax, rax
	mov	word ax, [current_y]
	inc	ax
	xor	rbx, rbx
	mov	word bx, [current_x]
	inc	rbx
	call	editor_movecursor
	ret
	
editor_movecursor:
	;; Uses esc_move_cursor buffer to send "\27[000;000H", after printfing out
	;; the current x and y

	push	rbx		; Preserve bx
	mov	rdi, esc_move_cursor
	mov	byte [rdi], 27
	inc	rdi
	mov	byte [rdi], '['
	inc 	rdi
	;; xor	rax, rax
	;; mov	word ax, [current_y]
	;; inc	ax
	call	print_int_strz
	mov	byte [rdi], ';'
	inc	rdi
	;; xor	rax, rax
	;; mov	word ax, [current_x]
	;; inc	ax
	pop	rax		; Current x we pushed as $rbx
	call	print_int_strz
	mov	byte [rdi], 'H'
	inc	rdi
	mov	byte [rdi], 0
	mov	rsi, esc_move_cursor
editor_movecursor_0:	
	call	ob_append_buffer
	ret
	
editor_homecursor:	
	mov	rsi, esc_homecursor
	call	ob_append_buffer
	ret
	
editor_cls:
	mov	rsi, esc_clearscreen
	call	ob_append_buffer
	ret

editor_show_pos:
	xor	rax, rax
	mov	ax, word [screen_max_y]
	inc	ax
	mov	rbx, 0x01
	call	editor_movecursor

	mov	rdi, esc_work
	xor	rax, rax
	mov	ax, word [current_x]
	call	print_int_strz
	mov	rsi, esc_work
	call	ob_append_buffer
	mov	rsi, ed_str_space
	call	ob_append_buffer

	mov	rdi, esc_work
	xor	rax, rax
	mov	ax, word [current_y]
	call	print_int_strz
	mov	rsi, esc_work
	call	ob_append_buffer
	mov	rsi, ed_str_space
	call	ob_append_buffer

	;; Print out last key
	mov	rdi, esc_work
	xor	rax, rax
	mov	ax, word [last_key]
	call	print_int_strz
	mov	rsi, esc_work
	call	ob_append_buffer
	mov	rsi, ed_str_space
	call	ob_append_buffer
	ret
	
draw_rows:
	mov	rcx, 0
draw_rows_0:
	push	rcx

draw_rows_normal_line:	
	mov	rdi, esc_work
	mov	rax, rcx
	push	rax
	push	rcx


	mov	rsi, ed_tilde
	call	ob_append_buffer

	pop	rcx
	pop	rax

	mov	rsi, esc_clearrow
	call	ob_append_buffer

	;; next row
	pop	rcx
	inc	rcx
	cmp	cx, word [screen_max_y]
	jl	draw_rows_0

draw_rows_done:
	ret

editor_handle_key:
	cmp	al, 27		; Esc
	jnz	hk_not_escape
	inc	byte [editor_key_len] ; Once this is >0 then we are capturing an escape sequence.
hk_not_escape:
	cmp	byte [editor_key_len], 0
	jz	hk_not_in_escape 
	mov	rcx, [editor_key_len]
	and	rcx, 0xff

	;; push	rax
	;; push	rcx
	;; call	editor_disp_key
	;; pop	rcx
	;; pop	rax
	
	mov	byte [editor_key_buffer + rcx], al
	inc	byte [editor_key_len]

	cmp	byte [editor_key_len], 0x04 	; esc codes are 3 chars
	jz	hk_in_escape

hk_loop_getch:
	call 	getch
	cmp	al, 0
	jz	hk_loop_getch

	jmp	editor_handle_key

hk_in_escape:
	call	hk_map_esc_seq
					; reset key buffer
	mov	qword [editor_key_buffer], 0x00000000
	mov	byte [editor_key_len], 0x00
	ret
hk_not_in_escape:
	cmp	al, ctrl_key('q')
	jnz     hk_next_1
	call 	exit_all
hk_next_1:
	ret

;;; Look up esc seq
hk_map_esc_seq:
	
	cmp	byte [editor_key_buffer + 2], '['
	jnz	hk_map_esc_seq_1
	mov	al, byte [editor_key_buffer + 3]
	mov	rsi, esc_seq_map
	mov	byte [last_key], al

hk_map_esc_seq_loop_0:	

	cmp	byte [rsi], 0x00
	je	hk_map_esc_seq_2

	cmp	byte al, [rsi]

	jnz	hk_map_esc_seq_loop_1 ; No match
	mov	word ax, [rsi+1]      ; Put the key enum (word) in ax
	call	[rsi+3]		      ; Call the key handler
	ret
hk_map_esc_seq_loop_1:	
	add	rsi, esc_seq_len
	jmp 	hk_map_esc_seq_loop_0
hk_map_esc_seq_1:	
hk_map_esc_seq_2:		; If we don't get a match print it
	mov	byte [last_key], al
	ret

hk_cursor_down:
	mov	ax, word [current_y]
	mov	bx, word [screen_max_y]
	dec	bx
	;; cmp	ax, word [screen_max_y]
	cmp	ax, bx
	jge	hk_cursor_down_0
	inc	word [current_y]
hk_cursor_down_0:	
	ret

hk_cursor_up:
	mov 	ax, word [current_y]
	cmp 	ax, 0
	jle	hk_cursor_down_0
	dec	word [current_y]
hk_cursor_up_0:
	ret
	
hk_cursor_right:
	mov	ax, word [current_x]
	mov	bx, word [screen_max_x]
	dec 	bx
	cmp	ax, bx
	jge	hk_cursor_right_0
	inc	word [current_x]
hk_cursor_right_0:
	ret

hk_cursor_left:
	mov	ax, word [current_x]
	cmp	ax, 0
	jle	hx_cursor_left_0
	dec	word [current_x]
hx_cursor_left_0:
	ret

hk_cursor_home:
	mov	ax, 0
	mov	word [current_x], ax
	ret

hk_cursor_end:
	mov	bx, word [screen_max_x]
	dec	bx
	mov	[current_x], bx
	ret
	
hk_cursor_pgup:
	mov 	ax, 0
	mov	word [current_y], ax
	ret

hk_cursor_pgdn:
	mov	bx, word [screen_max_y]
	dec	bx
	mov	word [current_y], bx
	ret
	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Assumes key code in rax (al actually)
editor_disp_key:
;	push 	rax
;	mov	rsi, esc_move_stat
;	call	print_strz
				;	pop	rax
	call	print_int
	mov	rsi, ed_str_space
	call	print_strz
	ret
	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

ob_init_buffer:	
	mov	[output_buffer], byte 0
	ret
	
ob_output_buffer:
	mov 	rsi, output_buffer
	call	print_strz
	call	ob_init_buffer	; Reset the buffer
	ret

;;; rsi points to a strz to be appended to the end of the output buffer
ob_append_buffer:
	mov	rdi, output_buffer
ob_append_buffer_0:
	cmp	[rdi], byte 0
	jz 	ob_append_buffer_1
	inc	rdi
	jmp	ob_append_buffer_0
ob_append_buffer_1:
	call	strzcpy
	ret
	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

	section 	.bss

	editor_key_buffer	resb 5
	esc_move_cursor		resb 10
	esc_work		resb 128

	last_key		resb 3
	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

	section 	.data

	editor_key_len	dw	0x0000

	welcome 	db 	"OSCAR editor -- version 0.0.1", 13, 10, 0
	welcome_len	equ	$-welcome
	
	esc_clearscreen	db	27, "[2J", 0
	esc_clearrow	db	27, "[K", 0
	esc_homecursor	db	27, "[H", 0
	esc_cursor_off	db	27, "[?25l", 0
	esc_cursor_on	db	27, "[?25h", 0

	esc_move_stat	db	27, "[000;000H", 0

	ed_str_space	db	" ", 0
	ed_str_dollar	db	"$", 0

	current_x		dw	0
	current_y		dw 	0
	screen_max_x	dw	0
	screen_max_y	dw 	0

;;; Escape key sequence defines and map

	%define ARROW_KEY_DOWN	1000
	%define ARROW_KEY_UP	1001
	%define ARROW_KEY_LEFT	1002
	%define ARROW_KEY_RIGHT	1003
	%define ARROW_KEY_HOME	1004

	%define SCAN_KEY_PGUP	53
	%define SCAN_KEY_PGDN	54
        %define SCAN_KEY_END	70
        %define SCAN_KEY_HOME	72

	buffer		db	0, 0, 0, 0, 0
	
	;; Key handler jump table (of sorts)
	;; Key scan code, followed by the internal key code (not yet used)
	;; followed by the handler function.  Needs to be terminated a zero scan code, as this
	;; is scanned through.  If we don't need the internal key code, this code potentially be
	;; replaced by a pure jump table
	esc_seq_map	db	'A'
	                dw	ARROW_KEY_UP
			dq      hk_cursor_up
	esc_seq_len	equ	$-esc_seq_map

			db	'B'
	                dw	ARROW_KEY_DOWN
	                dq      hk_cursor_down

			db	'C'
	                dw	ARROW_KEY_RIGHT
	                dq      hk_cursor_right

			db	'D'
	                dw	ARROW_KEY_LEFT
	                dq      hk_cursor_left

			db	SCAN_KEY_HOME
	                dw	ARROW_KEY_HOME
	                dq      hk_cursor_home

			db	SCAN_KEY_END
	                dw	0x0000
	                dq      hk_cursor_end
	
			db	SCAN_KEY_PGUP
	                dw	0x0000
	                dq      hk_cursor_pgup

			db	SCAN_KEY_PGDN
	                dw	0x0000
	                dq      hk_cursor_pgdn

			db	0x00                  ; scan code
			dw	0x0000                ; internal key code
			dq	0x00000000            ; handler
	

