;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Graceful shutdown
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

	%include "editor.def"
	%include "io.def"
	%include "termios.def"
	%include "exit.def"
	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

	section .text
	
exit_all:
	call	ob_init_buffer
	call 	editor_cls
	call 	editor_homecursor
	call	ob_output_buffer
	call 	restore_terminal
	mov	rsi, goodbye
	call	print_strz
	mov 	rdi, rax
	mov 	rax, 0x1
	syscall

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	
	section .data

	goodbye db "bye", 13, 10, 0
