
	%include "macro.def"
	%include "io.def"
	%include "sys.def"

	section .text
	
;;; Expects the character to write to STDOUT in rax.
;;; Clobbers rax, rdx, rdi, rsi
putch:
	mov [write_buffer_short], rax
	mov rax, SYS_write
	mov rdi, STDOUT
	mov rsi, write_buffer_short
	mov rdx, 0x1
	syscall
	ret
	
;;; Expects the character to write to STDOUT in rax.
;;; Clobbers rax, rdx, rdi, rsi
;;; Returns char in rax (or zero on error)
getch:
	mov	byte [read_buffer_short], 0
	mov	rax, SYS_read
	mov 	rdi, STDIN
	mov 	rsi, read_buffer_short
	mov 	rdx, 0x1
	
	syscall
	jc 	getch_error
	
	mov 	rax, [read_buffer_short]
	ret
getch_error:	
	mov 	rax, 0		; On error return 0
	ret

;;; Print a CRLF
;;; Clobbers rax and anything else the putch clobbers
print_crlf:
	push	rdi
	mov	rsi, CRLF
	call 	print_strz
	pop	rdi
	ret

;;; Print a null terminated string, pointed to by rsi
print_strz:
	mov 	al, [rsi]
	cmp 	al, 0
	jz 	print_strz_exit
	push 	rsi
	call 	putch
	pop 	rsi
	inc 	rsi
	jmp 	print_strz
print_strz_exit:
	ret

;;; Print integer to string.  Integer to print is in $rax, string is
;;; in $rdi
print_int_strz:
	mov 	rcx, 0 		; How many digits
	mov	rbx, 10		; Radix
print_int_strz0:
	mov	rdx, 0
	div	rbx		; Dividend in rax, remainder in rdx
	add	rdx, '0'	; Turn rdx into a digit
	push	rdx
	inc	rcx		; Keep track of how many digits
	cmp	rax, 0
	jnz	print_int_strz0

print_int_strz1:
	pop	rax
	mov	byte [rdi], al
	inc	rbx
	inc	rdi
	loop 	print_int_strz1
	mov	byte [rdi], 0x00
	ret
	
;;; Integer to print is in rax
;;; Clobbers rax
print_int:	
	mov 	rcx, 0 		; How many digits
	mov	rbx, 10		; Radix
print_int_loop0:
	mov	rdx, 0
	div	rbx		; Dividend in rax, remainder in rdx
	add	rdx, '0'	; Turn rdx into a digit
	push	rdx
	inc	rcx		; Keep track of how many digits
	cmp	rax, 0
	jnz print_int_loop0

print_int_loop1:
	pop	rax
	push	rcx
	call	putch
	pop	rcx
	loop print_int_loop1
	ret

;;; Data
	section .bss
	
	read_buffer_short	resb 10
	write_buffer_short	resb 10
	
	section .data

	CRLF			db	13, 10, 0
