	%include "prelude.asm"
	%include "sys.def"
	%include "termios.def"
	%include "io.def"
	%include "editor.def"
	
	global _start

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	section .text
	
_start:
	;; Deal with command line arguments - although initially
	;; we only care about a single filename
	mov 	r12, [rsp] 		; Number of options
   	mov 	r13, rsp		; Point to before first option
	cmp	r12, 0x02 		; Only support one actual parameter
	jnz	error

	call 	init_terminal
	jc 	error

	call	init_editor

outer_loop:	
	call	refresh_screen
	
loop_getch:
	call 	getch
	cmp		al, 0
	jz		loop_getch

	call 	editor_handle_key ; key is in rax

	;; call	editor_disp_key	
	
	;; call	print_int
	;; call	print_crlf

	;;
	;; jmp	loop_getch

	jmp		outer_loop
	
error:
	mov rdi, 0x7f
error1:
	mov rax, 0x1
	syscall

;;; End of _start loop

	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	
	section .data

	goodbye db "bye", 13, 10, 0

	section	.bss

	current_filename resb 128
