;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; String (strz) functions
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

	%include "string.def"
	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

	section .text

;;; rsi: source
;;; rdi: destination
strzcpy:
	mov	al, [rsi]
	mov	[rdi], al
	cmp	al, 0
	jz strzcpy_0
	inc	rsi
	inc	rdi
	jmp strzcpy
strzcpy_0:
	ret
