;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Lower level system functions
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

	section .text

	%include "sys.def"

;;; ior function - creates the equivalent of the _IOR macro
;;; Expects
;;; NR in al, TYPE in ah
;;; SIZE in ebx
;;; DR in rcx - one of IOC_READ or IOC_WRITE
;;; Returns IOR value in rsi
ior:
	mov rsi, rax		; NR and TYPE are already in eax
	shl ebx, 16		; SIZE
	or esi, ebx
	or esi, ecx

	ret

