;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Terminal related functions
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

	%include "sys.def"
	%include "io.def"
	%include "sys.def"
	%include "termios.def"
	%include "data.def"
	
section .text

;;; Turn off echo
init_terminal:
	call 	tcgetattr
	;; Make a copy
	mov 	rdi, termios_org
	mov 	rsi, termios_c_iflag
	mov 	rcx, ssize_termios_struct
	rep 	movsb

	or dword [termios_c_cflag], termios_CS8
	
	mov ecx, (termios_ECHO | termios_ICANON | termios_ISIG | termios_IEXTEN)
	xor ecx, -1
	and ecx, [termios_c_lflag]
	mov [termios_c_lflag], ecx

	mov ecx, (termios_IXON | termios_ICRNL | termios_BRKINT | termios_INPCK | termios_ISTRIP)
	xor ecx, -1
	and ecx, [termios_c_iflag]
	mov [termios_c_iflag], ecx

	mov ecx, (termios_OPOST)
	xor ecx, -1
	and ecx, [termios_c_oflag]
	mov [termios_c_oflag], ecx

	mov byte [termios_c_cc+termios_cc_VMIN], 0
	mov byte [termios_c_cc+termios_cc_VTIME], 1
	
	call tcsetattr
	ret

;;; Copy the original termios struct back into the attributes
restore_terminal:
	mov al, ioctl_TIOCSETA
	mov ah, 't'
	mov ebx, ssize_termios_struct
	mov ecx, IOC_WRITE
	call ior

	mov rax, SYS_ioctl
	mov rdi, STDIN
	mov rdx, termios_org
	syscall
	ret

;;; Retrieve the current terminal/window size using an ioctl call.
;;; Returns the rows in the lower 16 bits of rax, and columns in the
;;; next 16
getwinsz:
	mov 	al, ioctl_TIOCGWINSZ
	mov 	ah, 't'
	mov 	ebx, ssize_winsize_struct
	mov 	ecx, IOC_READ
	call 	ior
	
	mov 	rax, SYS_ioctl
	mov 	rdi, STDOUT
	mov 	rdx, winsize_ws_row	; Beginning of the winsize struct
	syscall

	jc		getwinsz_error

	xor		rax, rax
	mov		eax, dword [winsize_ws_row]

	;; Drop down rows and cols
	;; dec		word [winsize_ws_row]
	;; dec		word [winsize_ws_col]

	ret
	
getwinsz_error:
	mov	rax, 0
	mov	rbx, 0
	ret
	

tcsetattr:
	mov al, ioctl_TIOCSETA
	mov ah, 't'
	mov ebx, ssize_termios_struct
	mov ecx, IOC_WRITE
	call ior

	mov rax, SYS_ioctl
	mov rdi, STDIN
	mov rdx, termios_c_iflag
	syscall
	ret
	
	;; Call ioctl for tcgetattr.  Check the carry flag on return
	;; for an error.  Uses the global data structure
tcgetattr:
	mov al, ioctl_TIOCGETA
	mov ah, 't'
	mov ebx, ssize_termios_struct
	mov ecx, IOC_READ
	call ior		 ; Result will be in rsi
	
	mov rax, SYS_ioctl 	 ;
	mov rdi, STDIN		 ;
	mov rdx, termios_c_iflag ; The beginning of the termios struct
	syscall
	ret

