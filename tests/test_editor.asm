

	%include "prelude.asm"
	%include "editor.def"
	%include "io.def"
	
	global _start

	section .text

_start:

	call	ob_init_buffer

	mov	rsi, string1
	call	ob_append_buffer
	mov	rsi, string1
	call	ob_append_buffer

	call	ob_output_buffer

	mov 	rdi, rax
	mov 	rax, 0x1
	syscall

	section .data

	string1	db "hello", 13, 10, 0
	
	
