

	%include "prelude.asm"
	%include "string.def"
	%include "io.def"
	
	global _start

	section .text

_start:

	mov	rsi, string1
	mov	rdi, string2
	call	strzcpy

	mov 	rsi, string2
	call	print_strz
	
	mov 	rdi, rax
	mov 	rax, 0x1
	syscall

	section .data

	string1	db "hello", 13, 10, 0
	

	section .bss

	string2  resb 20
	
