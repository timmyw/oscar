draw_rows:
	mov		rcx, 0
draw_rows_0:
	cmp		cx, word [winsize_ws_row]
	jz		draw_rows_done
	inc		rcx
	push	rcx                        ; Keep rcx for later
	mov		r8, rcx

	mov		edx, 0
	xor		rax, rax
	mov		ax, word [winsize_ws_row]
	mov		ecx, 0x03                  ; Welcome message goes 1/3 down
	div		ecx
	cmp		r8, rax
	jnz		draw_rows_normal_line

	mov		ax, word [winsize_ws_col]  ; (cols - welcome_len)/2
	sub		ax, welcome_len
	shr		ax, 1

draw_rows_welcome_padding_0:               ; pad with spaces
	push	rax
	mov		rsi, ed_str_space
	call	ob_append_buffer
	pop		rax
	dec		ax
	jnz 	draw_rows_welcome_padding_0

	mov		rsi, welcome
	call	ob_append_buffer

	jmp		draw_rows_end_of_line

draw_rows_normal_line:
	mov		rsi, ed_tilde
	call	ob_append_buffer
draw_rows_end_of_line:
	mov	rsi, esc_clearrow
	call	ob_append_buffer
	pop	rcx

	jmp draw_rows_0
draw_rows_done:
	ret
