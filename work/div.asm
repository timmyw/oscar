;;; OpenBSD ELF header
	%ifdef OpenBSD
	section .note.openbsd.ident note
        align   2
        dd      8,4,1
        db      "OpenBSD",0
        dd      0
        align   2
	%endif

	global	_start
	
	section .text

_start:
	mov	rax, 100
	mov	ecx,   3
	div	ecx
	
	mov 	rdi, rax
	mov 	rax, 0x1
	syscall
	
