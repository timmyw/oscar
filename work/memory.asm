;;; OpenBSD ELF header
	%ifdef OpenBSD
	section .note.openbsd.ident note
        align   2
        dd      8,4,1
        db      "OpenBSD",0
        dd      0
        align   2
	%endif

	global	_start

	%ifdef OpenBSD
	%define	SYSCALL_MMAP	49
	%define PROT_NONE	0x00
	%define	PROT_READ	0x01
	%define	PROT_WRITE	0x02
	%define	PROT_EXEC	0x04

	%define	MAP_SHARED	0x0001
	%define	MAP_PRIVATE	0x0002

	%define MAP_ANONYMOUS	0x1000

	%else

	%define	SYSCALL_MMAP	9
	
	%define PROT_NONE	0x00
	%define	PROT_READ	0x01
	%define	PROT_WRITE	0x01
	%define	PROT_EXEC	0x04

	%define	MAP_SHARED	0x0001
	%define	MAP_PRIVATE	0x0002

	%define MAP_ANONYMOUS	0x0020
	
	%endif

	%define	BLOCK_SIZE	1024

	section .text

_start:

	;; syscall: "mmap"
	;; ret: "void *"
	;; args: "void *" "size_t" "int" "int" "int" "off_t"

	mov	rax, SYSCALL_MMAP

	;; OpenBSD
	;; RDI, RSI, RDX, RCX, R8, R9

	%ifdef OpenBSD

	;; mmap(0,0xaaaa,0x3<PROT_READ|PROT_WRITE>,0x1002<MAP_PRIVATE|MAP_ANON>,-1,0)
	mov	rdi, 0		               ; address
	mov	rsi, 0xaaaa		       ; size
	mov	rdx, 0x3		       ; protection
	mov	rcx, 0x1002		       ; flags
	mov	r8, -1			       ; fd
	mov	r9, 0			       ; offset

	syscall

	;; Linux
	%else
	mov	rdi, 0		               ; address
	mov	rsi, BLOCK_SIZE		       ; size
	mov	rdx, PROT_READ|PROT_WRITE      ; 
	mov	r10, MAP_PRIVATE|MAP_ANONYMOUS ; 
	mov	r8, -1			       ;
	mov	r9, 0			       ;
	syscall
	%endif
	
	mov 	rdi, rax
	mov 	rax, 0x1
	syscall

	section .data

	memory	dq	0x00000000
