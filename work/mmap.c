#include <stdio.h>
#include <sys/mman.h>

int main(int argc, char* argv[])
{
  void* addr	= 0;
  size_t len	= 0xaaaa;
  int prot	= PROT_READ | PROT_WRITE;
  int flags	= MAP_ANONYMOUS | MAP_PRIVATE;
  int fd	= -1;
  off_t offset	= 0;

  void *result;

  result = mmap(addr, len, prot, flags, fd, offset);
  printf("%lx\n", (long) result);

  munmap(result, len);

  return 0;
}
