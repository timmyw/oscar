#include <sys/mman.h>
#include <termios.h>
#include <stdio.h>

int main() {
  /* struct termios t; */
  /* tcgetattr(0, &t); */
  /* printf("%u\n", sizeof(unsigned short)); */
  /* printf("c_iflag = %08x\nc_oflag = %08x\nc_cflag = %08x\nc_lflag = %08x\n", */
  /* 	 t.c_iflag, t.c_oflag, t.c_cflag, t.c_lflag); */

  unsigned size = 16384;
  void * p = mmap(0, size, PROT_READ|PROT_WRITE, MAP_PRIVATE|MAP_ANONYMOUS, -1, 0);
  printf("p: %08x\n", p);
}
